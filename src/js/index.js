import polifill from "@babel/polyfill";
import Search from "./model/Search";
import { elements, renderLoader, clearLoader } from "./view/base";
import * as searchView from "./view/searchView";

// WEB App төлөв
// Хайлтын Query үр дүн
//  тухайн үзүүлж байгаа жор
//  лайкалсан жорууд
//  захиалж байгаа жорын  найрлаганууд
const state = {};

const controlSearch = async () => {
  console.log("дарагдлаа өоөоөоөо");
  // 1) вебээс хайлтын түлхүүр үгийг гаргаж авна.
  const query = searchView.getInput();
  if (query) {
    // 2) шинээр хайлтын обьектыг үүсгэж өгнө.
    state.search = new Search(query);
    // 3) хайлт хийхэд зориулж интерфейсийг бэлдэнэ.
    searchView.clearSearchQuery();
    searchView.clearSearchResult();
    renderLoader(elements.searchResultDiv);
    //  4) Хайлтыг гүйцэтгэнэ
    await state.search.doSearch();
    //  5) Хайлтын үр дүнг дэлгэцэнд гаргана аа
    clearLoader();
    if (state.search.result === undefined) alert("Хайлтаар илэрцгүй");
    searchView.renderRecipes(state.search.result);
  }
};

elements.searchForm.addEventListener("submit", (e) => {
  e.preventDefault();
  controlSearch();
});
