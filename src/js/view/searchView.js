import { elements } from "./base";
export const getInput = () => elements.searchInput.value;

//  Private function
const renderRecipe = (recipe) => {
  //     image_url: "http://forkify-api.herokuapp.com/images/yum8ee6.jpg"
  // publisher: "The Pioneer Woman"
  // publisher_url: "http://thepioneerwoman.com"
  // recipe_id: "c465d3"
  // social_rank: 99.99999996527706
  // source_url: "http://thepioneerwoman.com/cooking/2013/05/quick-and-easy-roasted-red-pepper-pasta/"
  // title: "Quick and Easy Roasted Red Pepper Pasta"
  console.log(recipe);
  const markup = ` 
    <li>
        <a class="results__link " href="#${recipe.recipe_id}">
            <figure class="results__fig">
                <img src="${recipe.image_url}" alt="Test" />
                    </figure>
                        <div class="results__data">
                            <h4 class="results__name">${recipe.title}</h4>
                                <p class="results__author">${recipe.publisher}</p>
                      </div>
                 </a>
            </li>`;
  //  UL tag руу нэмнэ ээ
  elements.searchResultList.insertAdjacentHTML("beforeend", markup);
};
export const clearSearchQuery = () => {
  elements.searchInput.value = "";
};
export const clearSearchResult = () => {
  elements.searchResultList.innerHTML = "";
};
export const renderRecipes = (recipes) => {
  recipes.forEach(renderRecipe);
};
